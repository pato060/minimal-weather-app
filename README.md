# Minimal Weather App

This is a Minimal Weather App assignment.

## Instructions

* Go inside Minimal-Weather-App folder
* Run "npm install" to install all dependencies required for running the app
* Run "ng serve" to run the application