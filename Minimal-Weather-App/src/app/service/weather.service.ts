import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {
  url: string = "http://api.openweathermap.org/data/2.5/forecast";
  apiKey: string = "fc2eb2f6286464e1c40560f4fc245d04";

  constructor(private http: HttpClient) { }

  getWeatherDataByID(cityId: number): Observable<any> {
    let params = new HttpParams()
    .set("id", cityId)
    .set("units", "metric")
    .set("cnt", 25)
    .set("appid", this.apiKey)

    return this.http.get(this.url, { params });
  }

  getWeatherDataByCoordinates(lat: number, lon: number): Observable<any> {
    let params = new HttpParams()
    .set("lat", lat)
    .set("lon", lon)
    .set("units", "metric")
    .set("cnt", 25)
    .set("appid", this.apiKey)

    return this.http.get(this.url, { params });
  }
}
