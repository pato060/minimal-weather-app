import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl } from '@angular/forms';
import { faMapMarkerAlt } from '@fortawesome/free-solid-svg-icons';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

@Component({
  selector: 'app-weather-locations',
  templateUrl: './weather-locations.component.html',
  styleUrls: ['./css/weather-locations.component.scss']
})
export class WeatherLocationsComponent implements OnInit {
  @Input() locationsList: any[] = [];
  @Output() selectLocationEvent = new EventEmitter();

  faMapMarkerAlt = faMapMarkerAlt;
  locationControl = new FormControl();
  
  filteredLocations: Observable<any[]>;

  constructor() { }

  ngOnInit(): void {
    this.filteredLocations = this.locationControl.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value))
    );
  }

  private _filter(value: string): any[] {
    const filterValue = value.toLowerCase();

    return this.locationsList.filter(option => option.locationName.toLowerCase().includes(filterValue));
  }

  selectLocation(location: any) {
    this.selectLocationEvent.emit(location);
  }
}
