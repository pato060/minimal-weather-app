import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { faMapMarkerAlt } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-city-weather-info',
  templateUrl: './city-weather-info.component.html',
  styleUrls: ['./css/city-weather-info.component.scss']
})
export class CityWeatherInfoComponent implements OnInit {
  @Input() selectedLocationObj: any;
  @Output() showLocationsEvent = new EventEmitter();

  faMapMarkerAlt = faMapMarkerAlt;

  constructor() { }

  ngOnInit(): void {
  }

  showLocations() {
    this.showLocationsEvent.emit();
  }
}
