import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { WeatherService } from 'src/app/service/weather.service';

@Component({
  selector: 'app-panel',
  templateUrl: './panel.component.html',
  styleUrls: ['./css/panel.component.scss'],
  animations: [
    trigger("showLocationsAnimation", [
      state('true', style({ top: '35px' })),
      state('false', style({ top: '*' })),
      transition('true => false', animate('400ms ease-in-out')),
      transition('false => true', animate('400ms ease-in-out'))
    ])
  ]
})
export class PanelComponent implements OnInit {

  showLocations: boolean = true;
  selectedLocation: any;
  weatherServiceSubscription: Subscription = new Subscription();
  locations: any[] =
  [
    {
      locationId: 3060972,
      locationName: 'Bratislava',
      temperature: ''
    },
    {
      locationId: 724627,
      locationName: 'Humenné',
      temperature: ''
    },
    {
      locationId: 0,
      lat: 48.72385729695343,
      long: 22.29725076340402,
      locationName: 'Koromľa',
      temperature: ''
    },
    {
      locationId: 724443,
      locationName: 'Košice',
      temperature: ''
    },
    {
      locationId: 724144,
      locationName: 'Michalovce',
      temperature: ''
    },
    {
      locationId: 723554,
      locationName: 'Sobrance',
      temperature: ''
    },
  ];

  constructor(private weatherService: WeatherService) { }

  ngOnInit(): void {
    this.locations.forEach( location => {
      if (location.locationId !== 0) {
        this.weatherServiceSubscription = this.weatherService.getWeatherDataByID(location.locationId).subscribe(result => {
          location.temperature = Math.round(result.list[0].main.temp).toString();
          location.sunrise = new Date(result.city.sunrise * 1000).toLocaleString('sk', { hour: 'numeric', minute: 'numeric', hour12: true });
          location.sunset = new Date(result.city.sunset * 1000).toLocaleString('sk', { hour: 'numeric', minute: 'numeric', hour12: true });
          location.time = new Date(result.list[0].dt_txt)
          .toLocaleDateString(
            'en-us', { weekday:"long",
            year:"numeric",
            month:"short",
            day:"numeric",
            hour: "numeric",
            minute: "numeric",
            hour12: true});
          location.weatherStatus = result.list[0].weather[0].main;
          location.maxTemp = Math.round(result.list[0].main.temp_max).toString();
          location.minTemp = Math.round(result.list[0].main.temp_min).toString();
          location.humidity = result.list[0].main.humidity;
          location.pressure = result.list[0].main.pressure;
          location.wind = Math.round(result.list[0].wind.speed * 3.6).toString();
          location.daytime = new Date((result.city.sunset * 1000) - (result.city.sunrise * 1000)).getHours().toString() + "h " + new Date(result.list[0].dt).getMinutes().toString() + "m";
          let currentDate = new Date();
          currentDate.setDate(currentDate.getDate() + 1);
          location.firstDayDate = currentDate.toLocaleDateString('en-us', { weekday:"short" }) + ", " +
           (currentDate.getDate() < 10 ? ("0" + currentDate.getDate()) : currentDate.getDate());

          currentDate = new Date();
          currentDate.setDate(currentDate.getDate() + 2);
          location.secondDayDate = currentDate.toLocaleDateString('en-us', { weekday:"short" }) + ", " +
          (currentDate.getDate() < 10 ? ("0" + currentDate.getDate()) : currentDate.getDate());

          currentDate = new Date();
          currentDate.setDate(currentDate.getDate() + 3);
          location.thirdDayDate = currentDate.toLocaleDateString('en-us', { weekday:"short" }) + ", " +
          (currentDate.getDate() < 10 ? ("0" + currentDate.getDate()) : currentDate.getDate());
          
          location.firstDayMaxTemp = Math.round(result.list[8].main.temp_max).toString();
          location.firstDayMinTemp = Math.round(result.list[8].main.temp_min).toString();

          location.secondDayMaxTemp = Math.round(result.list[16].main.temp_max).toString();
          location.secondDayMinTemp = Math.round(result.list[16].main.temp_min).toString();

          location.thirdDayMaxTemp = Math.round(result.list[24].main.temp_max).toString();
          location.thirdDayMinTemp = Math.round(result.list[24].main.temp_min).toString();

          location.firstDayWeatherStatus = result.list[8].weather[0].main;
          location.secondDayWeatherStatus = result.list[16].weather[0].main;
          location.thirdDayWeatherStatus = result.list[24].weather[0].main;
        });
      } else {
        this.weatherServiceSubscription = this.weatherService.getWeatherDataByCoordinates(location.lat!, location.long!).subscribe(result => {
          location.temperature = Math.round(result.list[0].main.temp).toString();
          location.sunrise = new Date(result.city.sunrise * 1000).toLocaleString('sk', { hour: 'numeric', minute: 'numeric', hour12: true });
          location.sunset = new Date(result.city.sunset * 1000).toLocaleString('sk', { hour: 'numeric', minute: 'numeric', hour12: true });
          location.time = new Date(result.list[0].dt_txt)
          .toLocaleDateString(
            'en-us', { weekday:"long",
            year:"numeric",
            month:"short",
            day:"numeric",
            hour: "numeric",
            minute: "numeric",
            hour12: true});
          location.weatherStatus = result.list[0].weather[0].main;
          location.maxTemp = Math.round(result.list[0].main.temp_max).toString();
          location.minTemp = Math.round(result.list[0].main.temp_min).toString();
          location.humidity = result.list[0].main.humidity;
          location.pressure = result.list[0].main.pressure;
          location.wind = Math.round(result.list[0].wind.speed * 3.6).toString();
          location.daytime = new Date((result.city.sunset * 1000) - (result.city.sunrise * 1000)).getHours().toString() + "h " + new Date(result.list[0].dt).getMinutes().toString() + "m";
          let currentDate = new Date();
          currentDate.setDate(currentDate.getDate() + 1);
          location.firstDayDate = currentDate.toLocaleDateString('en-us', { weekday:"short" }) + ", " +
           (currentDate.getDate() < 10 ? ("0" + currentDate.getDate()) : currentDate.getDate());

          currentDate = new Date();
          currentDate.setDate(currentDate.getDate() + 2);
          location.secondDayDate = currentDate.toLocaleDateString('en-us', { weekday:"short" }) + ", " +
          (currentDate.getDate() < 10 ? ("0" + currentDate.getDate()) : currentDate.getDate());

          currentDate = new Date();
          currentDate.setDate(currentDate.getDate() + 3);
          location.thirdDayDate = currentDate.toLocaleDateString('en-us', { weekday:"short" }) + ", " +
          (currentDate.getDate() < 10 ? ("0" + currentDate.getDate()) : currentDate.getDate());
          
          location.firstDayMaxTemp = Math.round(result.list[8].main.temp_max).toString();
          location.firstDayMinTemp = Math.round(result.list[8].main.temp_min).toString();

          location.secondDayMaxTemp = Math.round(result.list[16].main.temp_max).toString();
          location.secondDayMinTemp = Math.round(result.list[16].main.temp_min).toString();

          location.thirdDayMaxTemp = Math.round(result.list[24].main.temp_max).toString();
          location.thirdDayMinTemp = Math.round(result.list[24].main.temp_min).toString();

          location.firstDayWeatherStatus = result.list[8].weather[0].main;
          location.secondDayWeatherStatus = result.list[16].weather[0].main;
          location.thirdDayWeatherStatus = result.list[24].weather[0].main;
        });
      }
    });
  }

  toggleLocations() {
    this.showLocations = !this.showLocations;
  }

  getSelectedLocation(location: any) {
    this.selectedLocation = location;
    this.showLocations = false;
  }

  ngOnDestroy(): void {
    this.weatherServiceSubscription.unsubscribe();
  }
}
